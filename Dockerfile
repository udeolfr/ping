#Dockerhub
FROM node:7.7.2

#Image contenant Node.js 4.4.3 et le client Oracle 12.1 pour se connecter
#à une base Oracle 10.2 et plus
#utilisez le client 11.2 pour vous connecter à une base 9.2
#node-oracledb devra s'ajouter lors de l'exécution

RUN echo "America/Montreal" > /etc/timezone && dpkg-reconfigure -f noninteractive tzdata
#remplacer par la version la plus recente supportée par node-oracledb
MAINTAINER Malek Chioua

#Copier le client oracle 12.1
COPY ./docker/oracle /opt/oracle

RUN rm /bin/sh && ln -s /bin/bash /bin/sh && \
    printf "\\nalias ll=\"ls -la\"\\n" >> ~/.bashrc && \
    apt-get update -qq && \
    apt-get -y install unzip && \
    apt-get -y install libaio1 libaio-dev && \
    cd /opt/oracle && \
    unzip ./instantclient-basic-* && \
    unzip ./instantclient-sdk-* && \
    rm *.zip && \
    mv instantclient_12_1 instantclient && \
    cd /opt/oracle/instantclient && \
    ln -s libclntsh.so.12.1 libclntsh.so && \
    ln -s libocci.so.12.1 libocci.so

ENV LD_LIBRARY_PATH=/opt/oracle/instantclient
ENV PATH=/opt/oracle/instantclient:$PATH

#Copie de l app nodejs

ENV db_user=appsro
ENV db_password=ipr7roaps09
ENV db_connectString=finapps11:1523/IPR7

WORKDIR /mtl/app/indicateurs
COPY . /mtl/app/indicateurs
RUN cd /mtl/app/indicateurs

#pour builder en intranet
RUN npm config -g set cafile /mtl/app/indicateurs/cert/zeusii.ville.montreal.qc.pem

RUN npm install

EXPOSE 3017


EXPOSE 4000

#pour demarrer build
#CMD "/bin/bash"
CMD [ "npm", "start" ]