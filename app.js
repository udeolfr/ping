'use strict';
const express = require('express');
var SwaggerRestify = require('swagger-restify-mw');
var restify = require('restify');
var app = restify.createServer();

app.use(
  function crossOrigin(req,res,next){
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    return next();
  }
);

module.exports = app; // for testing

var port = process.env.PORT || 3017;

var config = {
  appRoot: __dirname // required config
};

SwaggerRestify.create(config, function(err, swaggerRestify) {
  if (err) { throw err; }

  swaggerRestify.register(app);

  app.listen(port);

});
