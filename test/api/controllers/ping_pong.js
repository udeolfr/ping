var should = require('should');
var request = require('supertest');
var server = require('../../../app');

describe('controllers', function() {

  describe('ping_pong', function() {

    describe('GET /ping', function() {

      it('should return a default string', function(done) {

        request(server)
          .get('/ping')
          .set('Accept', 'application/json')
          .expect('Content-Type', /json/)
          .expect(200)
          .end(function(err, res) {
            should.not.exist(err);

            res.body.should.eql('pong');

            done();
          });
      });

    });

  });

});
